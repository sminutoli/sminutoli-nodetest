/**
 * Created by sminutoli on 03/09/14.
 */
// make an object called mySon using Javascript Object Notation (JSON)
var mySon = {'name': 'Jason'};

// mySon's description is printed in the console
console.log(mySon);

// we see that mySon is an object
console.log(typeof mySon);

// object properties are accessed using dot notation
console.log(mySon.name);

// in javascript, objects also support this syntax which functions
// the same as dot notation with the benefit of being able to use
// a variable in the square brackets (array syntax) for dynamic property access
console.log(mySon['name']);

// we can use both the dot and the array syntax for property assignment
mySon['gender'] = 'male';
console.log('mySon.gender', mySon.gender);

// this is a library that is being included into the project.
// http object is built from the library and allows us to display web pages
var http = require('http');
http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'}); // this is a browser header with a status code of 200 ('success')
  res.end('Hello World\n'); // this would be document content.  i.e. <html><head></head><body></body></html> etc
}).listen(80); // start the server
console.log('Server listening on port 80');

// note that this file would be divided into multiple files,
// the server would have a routing system to determine which file to
// display, and all the files would likely pull content from a database.

// this is just the beginning!